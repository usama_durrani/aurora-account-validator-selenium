from settings.webdriver_settings import SeleniumDriver as sd
from time import sleep
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
import bs4, time, os, csv, requests

def get_accounts_frm_csv():
    with open('accounts.csv', 'r') as f:
        f_reader = csv.reader(f)
        lines = []
        for i in f_reader:
            if i not in lines:
                lines.append(i)
    return lines

def get_status_frm_csv():
    with open('status.csv', 'r') as f:
        f_reader = csv.reader(f)
        lines = []
        for i in f_reader:
            if i[0] not in lines:
                lines.append(i[0])
    return lines

def insert_status_csv(data):
    operation = 'w' if not os.path.exists('status.csv') else 'a'
    with open('status.csv', operation) as f:
        f_writer = csv.writer(f)
        f_writer.writerow(data)

def aurora_tester():
    browser = sd()
    driver = browser.get_driver_instance() # driver instance
    wait = browser.get_default_wait() # default wait instance
    statuses = get_status_frm_csv()

    for account in get_accounts_frm_csv():
        user_email, user_password = account[0], account[1]
        if user_email not in statuses:
            driver.get('https://carfax.com/Service/login')
                            
            wait.until(EC.presence_of_element_located((By.ID, 'email')))
            wait.until(EC.presence_of_element_located((By.ID, 'password')))
            email_field = driver.find_element_by_id('email')
            email_field.send_keys(user_email)
            sleep(0.5)

            pass_field = driver.find_element_by_id('password')
            pass_field.send_keys(user_password)
            sleep(0.5)

            # WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '//button[@type="submit"]')))
            login_btn = driver.find_element_by_xpath('//button[@type="submit"]')
            login_btn.click()
            try:
                wait.until(EC.presence_of_element_located((By.XPATH, '//div[@class="page"]')))
                insert_status_csv([user_email, user_password, 'works'])
                driver.get('https://www.carfax.com/Service/logout')
                sleep(10)
            except:
                insert_status_csv([user_email, user_password, 'fails'])



if __name__ == '__main__':
    aurora_tester()
    # data = tester('UHK134','NE')
    # print(data)