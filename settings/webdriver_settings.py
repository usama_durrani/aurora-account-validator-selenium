from selenium import webdriver
# import seleniumwire
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from common.helpers import Helpers
# from configs.paths import DRIVER_PATH
from settings.proxy_creds import PROXY_USER, PROXY_PASS, PROXY_HOST, PROXY_PORT
import random
import zipfile
# from database.models import proxiesStateModel
import chromedriver_autoinstaller

class SeleniumDriver:
    def __init__(self, logger=""):
        self.common = Helpers()
        self.user_agent = self.common.get_user_agent()
        # self.driver_path = DRIVER_PATH
        # self.proxy_db = proxiesStateModel()
        # self.use_proxy = True
        # if self.use_proxy: # uses proxy if set to true
        #     self.proxy = self.proxy_db.select_proxy() # gets proxy dictionary instance
        #     self.ip = self.proxy['http'].split('@')[-1].split(':')[0]
        #     self.port = self.proxy['http'].split('@')[-1].split(':')[1]

    def set_proxy(self, ip, port, username, password, use_proxy):
        "sets proxy configration for selenium"
        PROXY_HOST = ip
        PROXY_PORT = port
        PROXY_USER = username
        PROXY_PASS = password

        manifest_json = """
        {
            "version": "1.0.0",
            "manifest_version": 2,
            "name": "Chrome Proxy",
            "permissions": [
                "proxy",
                "tabs",
                "unlimitedStorage",
                "storage",
                "<all_urls>",
                "webRequest",
                "webRequestBlocking"
            ],
            "background": {
                "scripts": ["background.js"]
            },
            "minimum_chrome_version":"22.0.0"
        }
        """
        background_js = """
        var config = {
                mode: "fixed_servers",
                rules: {
                singleProxy: {
                    scheme: "http",
                    host: "%s",
                    port: parseInt(%s)
                },
                bypassList: ["localhost"]
                }
            };

        chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});

        function callbackFn(details) {
            return {
                authCredentials: {
                    username: "%s",
                    password: "%s"
                }
            };
        }

        chrome.webRequest.onAuthRequired.addListener(
                    callbackFn,
                    {urls: ["<all_urls>"]},
                    ['blocking']
        );
        """ % (PROXY_HOST, PROXY_PORT, PROXY_USER, PROXY_PASS)

        if use_proxy:
            pluginfile = 'proxy_auth_plugin.zip'

            with zipfile.ZipFile(pluginfile, 'w') as zp:
                zp.writestr("manifest.json", manifest_json)
                zp.writestr("background.js", background_js)

        return pluginfile

    def chrome_opts(self):
        "sets chrome options and settings"
        chrome_options = Options()
        # chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-sh-usage')
        # chrome_options.add_argument('--ingognito')
        # chrome_options.add_argument('--proxy-server=%s' % self.proxy)
        chrome_options.add_argument(f"user-agent={self.user_agent}")
        chrome_options.add_extension(self.set_proxy(PROXY_HOST, PROXY_PORT, PROXY_USER, PROXY_PASS, True))
        return chrome_options

    def get_driver_instance(self):
        "returns an instance of chrome driver"
        try:
            driver = webdriver.Chrome(options=self.chrome_opts())
        except Exception as e:
            print('downloading chrome driver...')
            chromedriver_autoinstaller.install()
            driver = webdriver.Chrome(options=self.chrome_opts())
        # driver = webdriver.Chrome(
        #     executable_path=self.driver_path, options=self.chrome_opts())
        driver.maximize_window()
        self.driver = driver
        return self.driver

    def get_default_wait(self):
        "returns default wait time for waiting periods"
        wait = WebDriverWait(self.driver, 40)
        return wait
